#Setup

##Windows

* Install [Node](https://nodejs.org/en/download/)
* Install [Python 2.7](https://www.python.org/downloads/)

###Then:

* pip install virtualenv
* Navigate to your project folder
* virtualenv env
