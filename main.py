#!python
import nflgame
from flask import Flask
from flask import jsonify
app = Flask(__name__)

@app.route("/")
def hello():
    games = nflgame.games(2013, week=1)
    players = nflgame.combine_game_stats(games)
    # for p in players.rushing().sort('rushing_yds').limit(5):
    #     msg = '%s %d carries for %d yards and %d TDs'
    return jsonify(players.rushing().sort('rushing_yds').limit(5))

if __name__ == "__main__":
    app.run()